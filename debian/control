Source: python-npx
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Drew Parsons <dparsons@debian.org>
Build-Depends: debhelper-compat (= 13),
 dh-python,
 pybuild-plugin-pyproject,
 flit (>= 3.2~),
 python3-setuptools,
 python3-all:any (>= 3.7~),
 python3-numpy (>= 1:1.20~)
Standards-Version: 4.7.0
Homepage: https://github.com/nschloe/npx
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-npx
Vcs-Git: https://salsa.debian.org/python-team/packages/python-npx.git
Testsuite: autopkgtest-pkg-python
Rules-Requires-Root: no

Package: python3-npx
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends}
Suggests: python-npx-doc
Description: extensions for NumPy (Python 3)
 NumPy is a large library used everywhere in scientific computing.
 That's why breaking backwards-compatibility comes at a significant
 cost and is almost always avoided, even if the API of some methods is
 arguably lacking. This package provides drop-in wrappers "fixing"
 those.
 .
 Provides alternative algorithms for dot, solve, sum_at/add_at,
 unique_rows, isin_rows, mean.
 .
 This package installs the library for Python 3.
